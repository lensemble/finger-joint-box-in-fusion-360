#Author-Ian Jobson.
#Description-Create a finger joint Box
#This script was created to work with Fusion360
#Date 05-2020
#Version 0.01 Alpha

import adsk.core, adsk.fusion, traceback, math

app = adsk.core.Application.get()
ui  = app.userInterface

newComp = None

def createNewComponent():
    # Get the active design.
    product = app.activeProduct
    design = adsk.fusion.Design.cast(product)
    rootComp = design.rootComponent
    allOccs = rootComp.occurrences
    newOcc = allOccs.addNewComponent(adsk.core.Matrix3D.create())
    return newOcc.component

def createBox():
    product = app.activeProduct
    design = adsk.fusion.Design.cast(product)
    if not design:
        ui.messageBox('It is not supported in current workspace, please change to MODEL workspace and try again.')
        return
    currentDesignType = design.designType

    #global newComp
    #newComp = createNewComponent()
    rootComp = design.rootComponent
    if rootComp is None:
        ui.messageBox('New component failed to create', 'New Component Failed')
        return

    # Use the current sketch
    sketches = rootComp.sketches
    #sketch = sketches.add(rootComp.xYConstructionPlane)
    sketch = app.activeEditObject
    if not type(sketch) is adsk.fusion.Sketch:
            ui.messageBox('A sketch needs to be active to run this command','No Active Sketch')
            return None

    # add sketch curves
    lines = sketch.sketchCurves.sketchLines
    numberSketch = sketch.profiles.item

    # Get the box parmeters from the user
    length_input = ui.inputBox("Enter the box length", 'BOX LENGTH', '12')
    length = float(length_input[0])
    width_input = ui.inputBox("Enter the box width", 'BOX WIDTH', '8')
    width = float(width_input[0])
    height_input = ui.inputBox("Enter the box height", 'BOX HEIGHT', '8')
    height = float(height_input[0])
    # For now the thickness of the material is going to be equal to the tab height - we an adjust later
    thickness_input = ui.inputBox("Enter the material thickness", 'MATERIAL THICKNESS', '0.5')
    thickness = float(thickness_input[0])
    # This is how many tabs we want on the long side of the box. We can calculate the tab width from here.
    tabcount_length_input = ui.inputBox("How Many Tabs on the length", 'NUMBER OF TABS OVER THE LENGTH', '5')
    tabcount_length = float(tabcount_length_input[0])
    # This is how many tabs we want on the short side of the box. We can calculate the tab width from here.
    tabcount_width_input = ui.inputBox("How Many Tabs on the width", 'NUMBER OF TABS OVER THE WIDTH', '3')
    tabcount_width = float(tabcount_width_input[0])
    tabcount_height_input = ui.inputBox("How Many Tabs on the height", 'NUMBER OF TABS OVER THE HEIGHT', '3')
    tabcount_height = float(tabcount_height_input[0])
    # This is how much we want the tabs to stick out from the joint.
    extralength_input = ui.inputBox("How Much Should Tabs Overhang", 'Amount of Overhang', '0')
    extralength = float(extralength_input[0])
    
   
    
    #The tabwidth should equal the total length - 2* the tab height divided by the tab count plus the gap count
    # 120-(2*tabheight)/(number of tabs + number of gaps)
    # Try length/((numberoffingers*2)+1)

    #tabwidth = (length - (2*thickness))/(tabcount_length + gapcount_length)
    tabwidth = (length-(2*thickness))/((tabcount_length*2)+1)
    tabwidthw = (width-(2*thickness))/((tabcount_width*2)+1)
    tabwidthh = (height-(2*thickness))/((tabcount_height*2)+1)
    tabheight = thickness + extralength

 # There should be one more gaps than tabs
    gapcount_length = tabcount_length + 1

    tabcountloop = 2*tabcount_length
    gapcountloop = 2*gapcount_length

    tabcountloop = int(tabcountloop)
    gapcountloop = int(gapcountloop)
    

    point0 = adsk.core.Point3D.create(tabheight, tabheight, 0)
    point1 = adsk.core.Point3D.create(tabheight, width - tabheight, 0)
    
    point = [0]
    lowpoint = [0]
    # This just loads the lists with enough entries for the box.
    for x in range (tabcountloop+5):
        point.append(x)
        lowpoint.append(x)
        pass
   
    # This creates the top Horizontal line of box of type 1
    # Get the points for all the tabs
    # This gets all the high points they are at the full width
    for tabs in range (1,tabcountloop+1):

        point[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, width, 0)
        pass

    # This gets all the low points they are at the width minus a tab height
    for tabs in range (1,gapcountloop+2):
        lowpoint[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, width - tabheight, 0)
        pass

    # These are the vertical lines
    for tabs in range (1,tabcountloop+1):
        lines.addByTwoPoints(point[tabs], lowpoint[tabs])
       
        pass
    point[0] = point1
    # These are the horizontal lines at the top
    for tabs in range (1,tabcountloop,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        
        pass
    # These are the horizontal lines at the bottom
    # lines.addByTwoPoints(point1, lowpoint[1])
    lowpoint[0] = point1
    for tabs in range (0,gapcountloop,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
        
        pass

 #This creates the bottom Horizontal line of box of type 1
    for tabs in range (1,tabcountloop+1):

        point[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, 0, 0)
        
        
        pass
    # This gets all the low points they are at the width minus a tab height
    for tabs in range (1,gapcountloop+2):
        lowpoint[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, 0 + tabheight, 0)
        pass

    # These are the vertical lines
    for tabs in range (1,tabcountloop+1):
        lines.addByTwoPoints(point[tabs], lowpoint[tabs])
        pass

    # These are the horizontal lines at the bottom of the side
    for tabs in range (1,tabcountloop,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        
        pass
    # These are the horizontal lines at the depth of the tab
    # lines.addByTwoPoints(point1, lowpoint[1])
    lowpoint[0] = point0
    for tabs in range (0,gapcountloop,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
        
        pass


###############################################################################################################################
##
##   The Vertical lines are created below here
##
###############################################################################################################################

# This creates the right hand Vertical line of box of type 1
    # The number of tabs and gaps can be calcualted !!What if its not a round number!!
    #tabcount_width = (width - (2*tabheight)) / tabwidth
    tabcount_width = int(tabcount_width*2)

    # The top of these tabs is at the full length.  The bottom is the length - the tabheight
    # We need to loop through the tabcount_width and create the points
    width_minustabheight = width - tabheight
    for tabs in range (0,tabcount_width+5):
        lowpoint[tabs] = adsk.core.Point3D.create(length-tabheight,width_minustabheight-(tabs*tabwidthw),0)
        point[tabs+1] = adsk.core.Point3D.create(length,width_minustabheight-(tabs*tabwidthw),0)
             
        pass
    # Then we need to draw the horizontal lines
    for tabs in range (1,tabcount_width+1):
        lines.addByTwoPoints(point[tabs+1], lowpoint[tabs])
        pass  
    #Now lets create the vertical lines
    for tabs in range (0,tabcount_width+2,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
       # lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    for tabs in range (2,tabcount_width+2,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
#

    # This creates the left hand Vertical line of box of type 1
    # The number of tabs and gaps can be calcualted !!What if its not a round number!!
    #tabcount_width = (width - (2*tabheight)) / tabwidth
    #tabcount_width = int(tabcount_width)

    # The top of these tabs is at zero length - The bottom is the tabheight
    # We need to loop through the tabcount_width and create the points
    width_tabheight = tabheight
    for tabs in range (0,tabcount_width+5):
        lowpoint[tabs] = adsk.core.Point3D.create(tabheight,width_tabheight+(tabs*tabwidthw),0)
        point[tabs+1] = adsk.core.Point3D.create(0,width_tabheight+(tabs*tabwidthw),0)
             
        pass
    # Then we need to draw the horizontal lines
    for tabs in range (1,tabcount_width+1):
        lines.addByTwoPoints(point[tabs+1], lowpoint[tabs])
        pass  
    #Now lets create the vertical lines
    for tabs in range (0,tabcount_width+2,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
       # lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    for tabs in range (2,tabcount_width+2,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    extrudeSide(thickness)
    ########################################################################################################
    ##
    ## This is where we will create the type 2 sides in a new sketch
    ##
    ##
    ########################################################################################################
    sketch = sketches.add(rootComp.xYConstructionPlane)
    
    # add sketch curves
    lines = sketch.sketchCurves.sketchLines
    numberSketch = sketch.profiles.item
    for tabs in range (1,tabcountloop+1):
        point[tabs] = adsk.core.Point3D.create((tabwidthh*tabs)+tabheight, width, 0)
        pass
    # This gets all the low points they are at the width minus a tab height
    for tabs in range (1,gapcountloop+2):
        lowpoint[tabs] = adsk.core.Point3D.create((tabwidthh*tabs)+tabheight, width - tabheight, 0)
        pass
    # These are the vertical lines
    for tabs in range (1,tabcountloop+1):
        lines.addByTwoPoints(point[tabs], lowpoint[tabs])
       
        pass
    # These are the horizontal lines at the top
    pointTest = adsk.core.Point3D.create(0,width,0)
    lines.addByTwoPoints(pointTest, point[1])
    for tabs in range (2,tabcountloop,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    #tabcountloop is the last point 
    
    pointOther = adsk.core.Point3D.create(length,width,0)
    lines.addByTwoPoints(point[tabcountloop], pointOther)
    # These are the horizontal lines at the bottom
    # lines.addByTwoPoints(point1, lowpoint[1])
    lowpoint[0] = point1
    for tabs in range (1,gapcountloop-1,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
        
        pass
    #####################################################################################
    ##
    ## This is the bottom horizontal line
    ##
    ######################################################################################
    for tabs in range (1,tabcountloop+1):
        point[tabs] = adsk.core.Point3D.create((tabwidthh*tabs)+tabheight, 0, 0)
        pass
    # This gets all the low points they are at the tab height
    for tabs in range (1,gapcountloop+2):
        lowpoint[tabs] = adsk.core.Point3D.create((tabwidthh*tabs)+tabheight, tabheight, 0)
        pass
    # These are the vertical lines
    for tabs in range (1,tabcountloop+1):
        lines.addByTwoPoints(point[tabs], lowpoint[tabs])
       
        pass
    # These are the horizontal lines at the top
    pointTest = adsk.core.Point3D.create(0,0,0)
    lines.addByTwoPoints(pointTest, point[1])
    for tabs in range (2,tabcountloop,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    #tabcountloop is the last point 
    
    pointOther = adsk.core.Point3D.create(length,0,0)
    lines.addByTwoPoints(point[tabcountloop], pointOther)
    # These are the horizontal lines at the bottom
    # lines.addByTwoPoints(point1, lowpoint[1])
    lowpoint[0] = point1
    for tabs in range (1,gapcountloop-1,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
        
        pass
    ###########################################################################################################
    ## Type 2 vertical lines below here
    ###########################################################################################################
    #tabcount_width = int(tabcount_width*2)

    # The top of these tabs is at the full length.  The bottom is the length - the tabheight
    # We need to loop through the tabcount_width and create the points
    width_minustabheight = width - tabheight
    for tabs in range (1,tabcount_width+5):
        lowpoint[tabs] = adsk.core.Point3D.create(length-tabheight,width_minustabheight-(tabs*tabwidthw),0)
        point[tabs+1] = adsk.core.Point3D.create(length,width_minustabheight-(tabs*tabwidthw),0)
             
        pass
    # Then we need to draw the horizontal lines
    for tabs in range (1,tabcount_width+1):
        lines.addByTwoPoints(point[tabs+1], lowpoint[tabs])
        pass  
    #Now lets create the vertical lines
    pointTR = adsk.core.Point3D.create(length,width,0)
    pointN = adsk.core.Point3D.create(length,width-tabheight-tabwidthw,0)
    lines.addByTwoPoints(pointTR, pointN)
    for tabs in range (1,tabcount_width+1,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
       # lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    for tabs in range (3,tabcount_width+1,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    pointBR = adsk.core.Point3D.create(length,0,0)
    lines.addByTwoPoints(pointBR, point[tabcount_width+1])
    #####################################################################################################
    # This is the left side vertical for type 2
    #####################################################################################################
    for tabs in range (1,tabcount_width+5):
        lowpoint[tabs] = adsk.core.Point3D.create(tabheight,width_tabheight+(tabs*tabwidthw),0)
        point[tabs+1] = adsk.core.Point3D.create(0,width_tabheight+(tabs*tabwidthw),0)
             
        pass
    # Then we need to draw the horizontal lines
    for tabs in range (1,tabcount_width+1):
        lines.addByTwoPoints(point[tabs+1], lowpoint[tabs])
        pass  
    #Now lets create the vertical lines
    for tabs in range (1,tabcount_width+1,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
       # lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    pointTR = adsk.core.Point3D.create(0,width,0)
    pointN = adsk.core.Point3D.create(0,width-tabheight-tabwidthw,0)
    lines.addByTwoPoints(pointTR, pointN)
    for tabs in range (3,tabcount_width+1,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    pointBR = adsk.core.Point3D.create(0,0,0)
    lines.addByTwoPoints(pointBR, point[2])
    #extrudeSide(thickness)
    #####################################################################################################
    ##
    ## This is where we will create the type 3 side
    ##
    ####################################################################################################
    sketch = sketches.add(rootComp.xYConstructionPlane)
    # add sketch curves
    lines = sketch.sketchCurves.sketchLines
    tabcount_height = int(tabcount_height)
    tabcount_height = 2*tabcount_height
    for tabs in range (1,tabcount_height+1):

        point[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, width, 0)
        pass

    # This gets all the low points they are at the width minus a tab height
    for tabs in range (1,tabcount_height+2):
        lowpoint[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, width - tabheight, 0)
        pass

    # These are the vertical lines
    for tabs in range (1,tabcount_height+1):
        lines.addByTwoPoints(point[tabs], lowpoint[tabs])
       
        pass
    point[0] = point1
    # These are the horizontal lines at the top
    for tabs in range (1,tabcount_height,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        
        pass
    # These are the horizontal lines at the bottom
    # lines.addByTwoPoints(point1, lowpoint[1])
    lowpoint[0] = point1
    pointTL3 = adsk.core.Point3D.create(0,height-tabheight,0)
    pointTR3 = adsk.core.Point3D.create(width,height-tabheight,0)
    for tabs in range (0,tabcount_height+2,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
        
        pass
    lines.addByTwoPoints(pointTL3,lowpoint[0])
    lines.addByTwoPoints(pointTR3,lowpoint[tabcount_height+1])

####################################################################################################################
## Bottom horizontal line is below here
####################################################################################################################
#This creates the bottom Horizontal line of box of type 1
    for tabs in range (1,tabcount_height+1):

        point[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, 0, 0)
        
        
        pass
    # This gets all the low points they are at the width minus a tab height
    for tabs in range (1,tabcount_height+2):
        lowpoint[tabs] = adsk.core.Point3D.create((tabwidth*tabs)+tabheight, 0 + tabheight, 0)
        pass

    # These are the vertical lines
    for tabs in range (1,tabcount_height+1):
        lines.addByTwoPoints(point[tabs], lowpoint[tabs])
        pass

    # These are the horizontal lines at the bottom of the side
    for tabs in range (1,tabcount_height,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        
        pass
    # These are the horizontal lines at the depth of the tab
    # lines.addByTwoPoints(point1, lowpoint[1])
    lowpoint[0] = point0
    pointBL3 = adsk.core.Point3D.create(0,tabheight,0)
    pointBR3 = adsk.core.Point3D.create(width,tabheight,0)
    for tabs in range (0,tabcount_height+2,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
        
        pass
    lines.addByTwoPoints(pointBL3,lowpoint[0])
    lines.addByTwoPoints(pointBR3,lowpoint[tabcount_height+1])
#######################################################################################################################
## This is the type 3 vertical side on the right
########################################################################################################################
# This creates the right hand Vertical line of box of type 1
    # The number of tabs and gaps can be calcualted !!What if its not a round number!!
    #tabcount_width = (width - (2*tabheight)) / tabwidth
    #tabcount_width = int(tabcount_width*2)

    # The top of these tabs is at the full length.  The bottom is the length - the tabheight
    # We need to loop through the tabcount_width and create the points
    width_minustabheight = width - tabheight
    for tabs in range (0,tabcount_width+6):
        lowpoint[tabs] = adsk.core.Point3D.create(height-tabheight,width_minustabheight-(tabs*tabwidthw),0)
        point[tabs+1] = adsk.core.Point3D.create(height,width_minustabheight-(tabs*tabwidthw),0)
             
        pass
    # Then we need to draw the horizontal lines
    for tabs in range (1,tabcount_width+1):
        lines.addByTwoPoints(point[tabs+1], lowpoint[tabs])
        pass  
    #Now lets create the vertical lines
    for tabs in range (1,tabcount_width+1,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
       # lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    for tabs in range (1,tabcount_width+2,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
#######################################################################################################################
## This is the type 3 vertical side on the left
########################################################################################################################

    for tabs in range (0,tabcount_width+6):
        lowpoint[tabs] = adsk.core.Point3D.create(tabheight,tabheight+(tabs*tabwidthw),0)
        point[tabs+1] = adsk.core.Point3D.create(0,tabheight+(tabs*tabwidthw),0)
             
        pass
    # Then we need to draw the horizontal lines
    for tabs in range (1,tabcount_width+1):
        lines.addByTwoPoints(point[tabs+1], lowpoint[tabs])
        pass  
    #Now lets create the vertical lines
    for tabs in range (1,tabcount_width+1,2):
        lines.addByTwoPoints(lowpoint[tabs], lowpoint[tabs+1])
       # lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass
    for tabs in range (1,tabcount_width+2,2):
        lines.addByTwoPoints(point[tabs], point[tabs+1])
        pass


    return
def extrudeSide(thickness):      
    # We will extrude the shapes once we complete the sketch
    product = app.activeProduct
    design = adsk.fusion.Design.cast(product)
    if not design:
        ui.messageBox('It is not supported in current workspace, please change to MODEL workspace and try again.')
        return
    currentDesignType = design.designType
    rootComp = design.rootComponent
    if rootComp is None:
        ui.messageBox('New component failed to create', 'New Component Failed')
        return
    sketch = app.activeEditObject
    if not type(sketch) is adsk.fusion.Sketch:
            ui.messageBox('A sketch needs to be active to run this command','No Active Sketch')
            return None
    profile = sketch.profiles.item(0)
    extrudes = rootComp.features.extrudeFeatures
    ext_input = extrudes.createInput(profile, adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
    distance = adsk.core.ValueInput.createByReal(thickness)
    ext_input.setDistanceExtent(False, distance)
    ext_input.isSolid = True
    extrudes.add(ext_input)
    return

    #endPt = bottomCenter.copy() #start from bottomCenter
    #endPt.y = bottomCenter.y + 8
    #heightLine = sketchlines.addByTwoPoints(bottomCenter, endPt)
    
    #endPt.x = endPt.x + 12
    #topLine = sketchlines.addByTwoPoints(heightLine.endSketchPoint, endPt)

    #endPt.y = endPt.y - 8
    #topLine = sketchlines.addByTwoPoints(heightLine.endSketchPoint, endPt)

    #endPt.x = endPt.x - 12
    #topLine = sketchlines.addByTwoPoints(heightLine.endSketchPoint, endPt)

   

def run(context):
    try:
        createBox()
        #extrudeSide()

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
